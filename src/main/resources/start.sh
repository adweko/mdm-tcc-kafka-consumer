#!/bin/sh

java -cp /opt/cvboostconnector/lib/*:/opt/cvboostconnector/MDMTCCKafkaConsumer.jar \
  -Dfile.encoding=UTF-8 \
  com.helvetia.tcc.connector.MDMTCCKafkaConsumer \
  -i CDC-KAFKATEST1 \
  -s ZEQ2KAFK \
  -c /opt/cvboostconnector/config.properties \
  -l INFO \
  -t "CDC.zdkub000.z00.tkupe_person, CDC.zdkub000.z00.tkupr_privatperson,  CDC.zdkub000.z00.tkuun_unternehmung, CDC.zdkub000.z00.tkuad_adresse, CDC.zdkub000.z00.tkumi_mitarbeiter, CDC.zdkub000.z00.tkupz_perszus, CDC.zdkub000.z00.tkucp_codezurpers, CDC.zdkub000.z00.tkupi_pezusatzinfo, CDC.zdkub000.z00.tkupf_fmaregister, CDC.zdkub000.z00.tkuko_kommelement, CDC.zdkub000.z00.tkukb_koelverwbez"  \
  -n 1 \
  -d

#  -f cdc.json \

