package com.datamirror.ts.kafka.txconsistentconsumer.bookmark;

import com.datamirror.ts.kafka.txconsistentconsumer.TxConsistentConsumerException;
import org.apache.kafka.common.header.Headers;
import org.apache.kafka.common.serialization.Deserializer;

import java.nio.ByteBuffer;
import java.util.Map;

public class BookmarkDeserializer implements Deserializer<Bookmark> {

    @Override
    public void configure(Map<String, ?> configs, boolean isKey) {

    }

    @Override
    public Bookmark deserialize(String topic, byte[] data) {
        try {
            return new PublicTxConsistentConsumerBookmarkV1().from(ByteBuffer.wrap(data));
        } catch (TxConsistentConsumerException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Bookmark deserialize(String topic, Headers headers, byte[] data) {
        return deserialize(topic, data);
    }

    @Override
    public void close() {

    }
}
