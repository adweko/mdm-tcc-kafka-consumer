package com.helvetia.tcc.response;

import com.fasterxml.jackson.annotation.*;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
        "workflowKey",
        "workflowDefinitionId",
        "workflowDeploymentId",
        "category",
        "system",
        "status",
        "name",
        "functionalStatus",
        "functionalStatusCode",
        "creator",
        "owner",
        "created",
        "updated",
        "deleted"
})
public class CvboostResponse {

    @JsonProperty("id")
    private String id;
    @JsonProperty("workflowKey")
    private String workflowKey;
    @JsonProperty("workflowDefinitionId")
    private String workflowDefinitionId;
    @JsonProperty("workflowDeploymentId")
    private String workflowDeploymentId;
    @JsonProperty("category")
    private String category;
    @JsonProperty("system")
    private String system;
    @JsonProperty("status")
    private String status;
    @JsonProperty("name")
    private String name;
    @JsonProperty("functionalStatus")
    private Object functionalStatus;
    @JsonProperty("functionalStatusCode")
    private Object functionalStatusCode;
    @JsonProperty("creator")
    private String creator;
    @JsonProperty("owner")
    private String owner;
    @JsonProperty("created")
    private String created;
    @JsonProperty("updated")
    private String updated;
    @JsonProperty("deleted")
    private Object deleted;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("workflowKey")
    public String getWorkflowKey() {
        return workflowKey;
    }

    @JsonProperty("workflowKey")
    public void setWorkflowKey(String workflowKey) {
        this.workflowKey = workflowKey;
    }

    @JsonProperty("workflowDefinitionId")
    public String getWorkflowDefinitionId() {
        return workflowDefinitionId;
    }

    @JsonProperty("workflowDefinitionId")
    public void setWorkflowDefinitionId(String workflowDefinitionId) {
        this.workflowDefinitionId = workflowDefinitionId;
    }

    @JsonProperty("workflowDeploymentId")
    public String getWorkflowDeploymentId() {
        return workflowDeploymentId;
    }

    @JsonProperty("workflowDeploymentId")
    public void setWorkflowDeploymentId(String workflowDeploymentId) {
        this.workflowDeploymentId = workflowDeploymentId;
    }

    @JsonProperty("category")
    public String getCategory() {
        return category;
    }

    @JsonProperty("category")
    public void setCategory(String category) {
        this.category = category;
    }

    @JsonProperty("system")
    public String getSystem() {
        return system;
    }

    @JsonProperty("system")
    public void setSystem(String system) {
        this.system = system;
    }

    @JsonProperty("status")
    public String getStatus() {
        return status;
    }

    @JsonProperty("status")
    public void setStatus(String status) {
        this.status = status;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("functionalStatus")
    public Object getFunctionalStatus() {
        return functionalStatus;
    }

    @JsonProperty("functionalStatus")
    public void setFunctionalStatus(Object functionalStatus) {
        this.functionalStatus = functionalStatus;
    }

    @JsonProperty("functionalStatusCode")
    public Object getFunctionalStatusCode() {
        return functionalStatusCode;
    }

    @JsonProperty("functionalStatusCode")
    public void setFunctionalStatusCode(Object functionalStatusCode) {
        this.functionalStatusCode = functionalStatusCode;
    }

    @JsonProperty("creator")
    public String getCreator() {
        return creator;
    }

    @JsonProperty("creator")
    public void setCreator(String creator) {
        this.creator = creator;
    }

    @JsonProperty("owner")
    public String getOwner() {
        return owner;
    }

    @JsonProperty("owner")
    public void setOwner(String owner) {
        this.owner = owner;
    }

    @JsonProperty("created")
    public String getCreated() {
        return created;
    }

    @JsonProperty("created")
    public void setCreated(String created) {
        this.created = created;
    }

    @JsonProperty("updated")
    public String getUpdated() {
        return updated;
    }

    @JsonProperty("updated")
    public void setUpdated(String updated) {
        this.updated = updated;
    }

    @JsonProperty("deleted")
    public Object getDeleted() {
        return deleted;
    }

    @JsonProperty("deleted")
    public void setDeleted(Object deleted) {
        this.deleted = deleted;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}