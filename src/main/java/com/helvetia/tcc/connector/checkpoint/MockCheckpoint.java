package com.helvetia.tcc.connector.checkpoint;

public class MockCheckpoint implements BookmarkCheckpoint {

    String checkpoint;

    @Override
    public void commit(String bookmark) {
        checkpoint = bookmark;
    }

    @Override
    public String getLatest() {
        return checkpoint;
    }
}
