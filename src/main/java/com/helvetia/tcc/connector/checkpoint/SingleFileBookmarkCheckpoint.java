package com.helvetia.tcc.connector.checkpoint;

import com.helvetia.tcc.connector.exceptions.CommitFailedException;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class SingleFileBookmarkCheckpoint implements BookmarkCheckpoint {

    private final String BOOKMARK_FILENAME;

    public SingleFileBookmarkCheckpoint(String bookMarkFileName) {
        this.BOOKMARK_FILENAME = bookMarkFileName;
    }

    @Override
    public void commit(String bookmark) {
        try (FileWriter fileWriter = new FileWriter(BOOKMARK_FILENAME)) {
            fileWriter.write(bookmark);
            fileWriter.flush();
        } catch (IOException e) {
            throw new CommitFailedException(e);
        }
    }

    @Override
    public String getLatest() {
        String bookmark = null;
        File readLCBFile = new File(BOOKMARK_FILENAME);
        try (FileReader reader = new FileReader(readLCBFile)) {
            char[] cs = new char[104];
            reader.read(cs, 0, 104);
            bookmark = "";
            for (char c : cs) {
                bookmark = bookmark + c;
            }
        } catch (Exception ignored) {
        }
        return bookmark;
    }
}
