package com.helvetia.tcc.connector.checkpoint;

/**
 * Will implement safety backup file checkpoints, in case writing to one file fails.
 * Alternate constantly between two files, so that we always can go back to the last one in case
 * the other is corrupted.
 * TODO: implementation
 */
public class MultiFileBookmarkCheckpoint implements BookmarkCheckpoint {

    @Override
    public void commit(String bookmark) {

    }

    @Override
    public String getLatest() {
        return null;
    }
}
