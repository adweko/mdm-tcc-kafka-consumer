package com.helvetia.tcc.connector.checkpoint;

/**
 * Interface to implement a checkpointing of bookmarks.
 * When a new transaction has been processed, call {@link #commit(String bookmark)}.
 * To initialize to the initial or the latest value, call {@link #getLatest()};
 */
public interface BookmarkCheckpoint {
    void commit(String bookmark);
    String getLatest();
}
