
package com.helvetia.tcc.connector;

import com.datamirror.ts.kafka.txconsistentconsumer.TopicRecordIf;
import com.datamirror.ts.kafka.txconsistentconsumer.TxConsistentConsumerConfiguration;
import com.datamirror.ts.kafka.txconsistentconsumer.TxConsistentConsumerException;
import com.datamirror.ts.kafka.txconsistentconsumer.api.TxConsistentConsumer;
import com.datamirror.ts.kafka.txconsistentconsumer.api.TxConsistentConsumerImpl;
import com.datamirror.ts.kafka.txconsistentconsumer.bookmark.Bookmark;
import com.datamirror.ts.kafka.txconsistentconsumer.bookmark.TxConsistentConsumerBookmarkBuilder;
import com.datamirror.ts.properties.TsProperties;
import com.datamirror.ts.properties.TsPropertyException;
import com.datamirror.ts.util.trace.Trace;
import com.helvetia.tcc.connector.checkpoint.BookmarkCheckpoint;
import com.helvetia.tcc.connector.checkpoint.SingleFileBookmarkCheckpoint;
import com.helvetia.tcc.connector.exceptions.ConnectorRuntimeException;
import com.helvetia.tcc.connector.model.OperationCounter;
import com.helvetia.tcc.connector.target.CVBoostTarget;
import com.helvetia.tcc.connector.target.ConnectorTarget;
import com.helvetia.tcc.connector.target.LocalFileTarget;
import io.confluent.kafka.serializers.KafkaAvroDeserializer;
import io.confluent.kafka.serializers.KafkaAvroDeserializerConfig;
import io.confluent.kafka.serializers.KafkaAvroSerializerConfig;
import org.apache.commons.cli.*;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;


/**
 * <pre>{@code
 * This is an Transactionally consistent consumer sample. The sample dumps records read by Transactionally consistent consumer to the system output.
 *
 * To run the sample from CDC installation folder type:
 *   java -cp "lib/*" com.datamirror.ts.kafka.txconsistentconsumer.sampleapplications.AvroRest -i <CDC instance name> -s <subscription name> -c <fully qualified consumer configuration file name> -d
 *
 * To run the sample from any folder other than CDC installation folder:
 *   1) copy following jar's from <CDC installation>/lib folder into the folder sample will run from
 *        common-utils-3.0.0.jar
 *        kafka-avro-serializer-3.0.0.jar
 *        common-config-3.0.0.jar
 *        kafka-clients-0.10.0.0-cp1.jar
 *        jackson-core-2.6.3.jar
 *        jackson-annotations-2.6.0.jar
 *        jackson-databind-2.6.3.jar
 *        ts.jar
 *        commons-cli.jar
 *        avro-1.7.7.jar
 *        jackson-core-asl-1.9.13.jar
 *        jackson-mapper-asl-1.9.13.jar
 *        slf4j-log4j12-1.7.21.jar
 *        slf4j-api-1.7.21.jar
 *        log4j-1.2.17.jar
 *        kafka-schema-registry-client-3.0.0.jar
 *   2) java -Dfile.encoding=UTF-8 -cp "*" com.helvetia.tcc.rest.AvroRest -i <CDC instance name> -s <subscription name> -c <fully qualified consumer configuration file name> -d
 *       * file encoding has to be set to UTF8 specific for windows systems
 * Configuration file:
 *   bootstrap.servers=PLAINTEXT://<kafka broker host>:<kafka broker port>
 *   schema.registry.url=http://dlepniakov3vm:8081
 *   security.protocol=<SSL>
 *   ssl.endpoint.identification.algorithm=https
 *   ssl.key.password=<password>
 *   ssl.keystore.location=<jks keystore path>
 *   ssl.keystore.password=<password>
 *   ssl.keystore.type=JKS
 *   ssl.protocol=TLS
 *   ssl.trustmanager.algorithm=PKIX
 *   ssl.truststore.location=<jks truststore location>
 *   ssl.truststore.password=<password>
 *   ssl.truststore.type=JKS
 *   group.id=<group id>
 *   client.id=<client id>
 *   cvboost.workorder.url=HTTP://<host>:<port>/cvboost/services/rest/processing/workorder/v1.0/start/<workorder name>
 *   cvboost.workorder.file=<filename>
 *   cvboost.username=<username>
 *   cvboost.password=<password>
 *
 * The following is full list of supported options:
 *   -i,--instance-name <arg>     CDC instance name
 *   -s,--sub-name <arg>          Subscription name
 *   -c,--consumer-config <arg>   Consumer configuration file
 *   -d,--dump                    Dump all records into console (if not specified, print out statistics only)
 *   -h,--help                    Show help
 *   -n,--max <arg>               Max number of Kafka consumers Transactionally consistent consumer is allowed to use
 *   -b,--bookmark <arg>          Start bookmark, bookmark ID or HEAD or TAIL
 *   -t,--topic <arg>              Topic List
 *   -w,--waits <arg>             Number of waits for data to be returned by Transactionally consistent consumer poll request
 *   -l,--loglevel <arg>          Set log4j loglevel to "ALL", "TRACE", "DEBUG", "INFO", "WARN", "ERROR", "FATAL", "OFF"
 *   -f,--file <filename>         "Write transactions as JSONs into file, do not call REST service
 * }</pre>
 * <p>
 * Source code for the sample can be found <a href="file:////cdcinstances/kafkatgt/samples/samples.jar">here</a>
 */

public class MDMTCCKafkaConsumer {
    // IBM-side variables
    private final TxConsistentConsumer<byte[], byte[]> tcc;
    private final Bookmark startBookmark;
    private final KafkaAvroDeserializer keyDeserializer;
    private final KafkaAvroDeserializer valueDeserializer;
    private final boolean dumpRecords;
    private int numberOfWaits;
    private final boolean checkForWaits;

    private final static String LAST_COMMITTED_BOOKMARK = "lastCommittedBookmark";

    private ConnectorTarget target;
    private final BookmarkCheckpoint checkpoint;

    /* Get actual class name to be printed on */
    private static final Logger log = Logger.getLogger(MDMTCCKafkaConsumer.class.getName());

    public MDMTCCKafkaConsumer(TxConsistentConsumer<byte[], byte[]> tcc,
                               Bookmark startBookmark,
                               boolean dumpRecords,
                               int numberOfWaits,
                               ConnectorTarget connectorTarget,
                               BookmarkCheckpoint checkpoint,
                               String pathTopropertiesFile
    ) {
        Properties consumerProps = new Properties();
        try (FileInputStream configFileStream = new FileInputStream(pathTopropertiesFile)) {
            consumerProps.load(configFileStream);
        } catch (IOException e) {
            Trace.traceAlways(e);
        }

        String schemaUrl = consumerProps.getProperty(KafkaAvroSerializerConfig.SCHEMA_REGISTRY_URL_CONFIG);
        this.tcc = tcc;
        this.numberOfWaits = numberOfWaits;

        checkForWaits = numberOfWaits > 0;
        Map<String, Object> keyDeserializerConfig = new HashMap<>();
        keyDeserializerConfig.put(KafkaAvroSerializerConfig.SCHEMA_REGISTRY_URL_CONFIG, schemaUrl);
        keyDeserializerConfig.put(KafkaAvroDeserializerConfig.SPECIFIC_AVRO_READER_CONFIG, "false");
        keyDeserializer = new KafkaAvroDeserializer();
        keyDeserializer.configure(keyDeserializerConfig, true);

        Map<String, Object> valueDeserializerConfig = new HashMap<>(keyDeserializerConfig);
        valueDeserializerConfig.put(KafkaAvroDeserializerConfig.SPECIFIC_AVRO_READER_CONFIG, "false");
        valueDeserializer = new KafkaAvroDeserializer();
        valueDeserializer.configure(valueDeserializerConfig, false);

        this.startBookmark = startBookmark;
        this.dumpRecords = dumpRecords;

        this.checkpoint = checkpoint;

        // CV Boost configuration
        String cvboostWorkOrderUrl = consumerProps.getProperty(Config.CVBoost.CVBOOST_WORKORDER_URL);
        String cvboostWorkOrderFile = consumerProps.getProperty(Config.CVBoost.CVBOOST_WORKORDER_FILE);
        String cvboostUserName = consumerProps.getProperty(Config.CVBoost.CVBOOST_USERNAME);
        String cvboostPassWord = consumerProps.getProperty(Config.CVBoost.CVBOOST_PASSWORD);
        // TODO: find better solution for initialization
        this.target = connectorTarget;
        if (this.target == null) {
            this.target = new CVBoostTarget(cvboostWorkOrderUrl, cvboostWorkOrderFile, cvboostUserName, cvboostPassWord);
        }

    }


    /**
     * Method uses Transactionally consistent consumer to poll records (see {@link TxConsistentConsumer#poll(long)}).
     *
     * @throws TxConsistentConsumerException can be thrown
     * @throws IOException                   can be thrown
     */
    public void consume() throws TxConsistentConsumerException, IOException, ConnectorRuntimeException {

        boolean stopDumping = false;
        long startTime = System.currentTimeMillis();
        long totalRunTime;
        RecordHandler recordHandler = new RecordHandler(target,
                checkpoint,
                keyDeserializer,
                valueDeserializer,
                dumpRecords);

        tcc.seek(startBookmark);
        try {
            while (!stopDumping) {
                List<TopicRecordIf<byte[], byte[]>> records = tcc.poll(5000L);
                if (records != null) {
                    recordHandler.handleRecords(records);
                } else if (checkForWaits) {
                    if (numberOfWaits == 0) {
                        stopDumping = true;
                    } else {
                        // TODO: This here decreases a "global" var. Should it not be reset when records come again?
                        numberOfWaits--;
                        try {
                            Thread.sleep(1000L);
                        } catch (InterruptedException ignored) {

                        }
                    }
                } // never stop console
            }
        } finally {
            totalRunTime = System.currentTimeMillis() - startTime;
            System.out.println("Number of Dml operations = " + OperationCounter.getInstance().getNumberOfDmlOps() + " Number of commit operations = "
                    + OperationCounter.getInstance().getNumberOfCommitOps() + " Time (ms): " + totalRunTime);
            if (!dumpRecords) {
                System.out.println("Number of Dml operations per sec = " + (OperationCounter.getInstance().getNumberOfDmlOps() / totalRunTime) * 1000);
            }
            tcc.close();
        }
    }


    /**
     * Java main method
     *
     * @param args the command arguments see {@link MDMTCCKafkaConsumer}
     * @throws TsPropertyException can be thrown
     */
    public static void main(String[] args) throws TsPropertyException, IOException, TxConsistentConsumerException {

        Thread shutdownHook = new Thread("cvboostconnector-shutdown-hook") {
            public void run() {
                System.out.println("cvboostconnector wird heruntergefahren...");
                System.out.println("cvboostconnector-Herunterfahren beendet.");
            }
        };

        Runtime.getRuntime().addShutdownHook(shutdownHook);

        Options options = buildCommandLineOptions();
        CommandLine cmd = parseCommandLine(args, options);
        if (cmd == null) {
            throw new IllegalStateException("command line options need to be supplied");
        }
        configureRootLogger(cmd);
        TxConsistentConsumerConfiguration
                apiConfig = initTxConsistentConsumerConfiguration(cmd);

        log.info("Starting CvBoostConnector");

        ConnectorTarget target = null;
        if (cmd.hasOption("file")) {
            // Set filename + path to write JSON file
            String fileName = cmd.getOptionValue("file");
            target = new LocalFileTarget(fileName);
        }

        BookmarkCheckpoint checkpoint = new SingleFileBookmarkCheckpoint(LAST_COMMITTED_BOOKMARK);
        Bookmark startBookmark = getStartingBookmark(cmd, apiConfig, checkpoint);

        int numberOfWaits = 0;
        if (cmd.hasOption("waits")) {
            numberOfWaits = ((Number) cmd.getOptionObject("waits")).intValue();
        }


        MDMTCCKafkaConsumer txConsistentConsumerStandaloneTest = new MDMTCCKafkaConsumer(
                new TxConsistentConsumerImpl<>(apiConfig),
                startBookmark,
                cmd.hasOption("dump"),
                numberOfWaits,
                target,
                checkpoint,
                cmd.getOptionValue("consumer-config")

        );
        txConsistentConsumerStandaloneTest.consume();
        System.out.println("done");
        log.info("CVBoostConnector finished");

    }

    private static Bookmark getStartingBookmark(CommandLine cmd, TxConsistentConsumerConfiguration apiConfig, BookmarkCheckpoint checkpoint) throws TxConsistentConsumerException {
        String lastCommittedBookmark = checkpoint.getLatest();

        Bookmark startBookmark;
        TxConsistentConsumerBookmarkBuilder bmBuilder = Bookmark.getBookmarkBuilder(
                cmd.getOptionValue("instance-name"),
                cmd.getOptionValue("sub-name"),
                apiConfig);
        if (cmd.hasOption("bookmark")) {
            String optionBookmark;
            optionBookmark = cmd.getOptionValue("bookmark");
            if (optionBookmark.equals("HEAD")) {
                startBookmark = bmBuilder.tail(); // tail() is OK for head, bug in tcc
            } else if (optionBookmark.equals("TAIL")) {
                startBookmark = bmBuilder.head(); // head() is OK for tail, bug in tcc
            } else {
                startBookmark = Bookmark.readExternal(optionBookmark);
            }
        } else if (lastCommittedBookmark != null) {
            startBookmark = Bookmark.readExternal(lastCommittedBookmark);
        } else {
            startBookmark = bmBuilder.tail(); // Begin from start, tail() is OK for head, bug in TCC
        }
        log.info("Start Bookmark: " + startBookmark.get());
        return startBookmark;
    }

    private static TxConsistentConsumerConfiguration initTxConsistentConsumerConfiguration(CommandLine cmd) throws TsPropertyException {
        Properties tccConfiguration = new Properties();
//    To enable full trace un-comment the following line
//      Trace.setTraceOnUntil(Clock.currentTimeMillis()+30000);
//    To enable trace output to console uncomment the following line
//      Trace.setEchoToConsole(true);
        tccConfiguration.setProperty("tx_consistent_consumer_config_file_path", cmd.getOptionValue("consumer-config"));
//      To specify topic names filters use tx_consistent_consumer_ignored_topics_list or tx_consistent_consumer_topics_list parameters
//      tccConfiguration.setProperty("tx_consistent_consumer_ignored_topics_list", "kafkatgt.dl71023a.sourcedb.db2inst.dl71023p01_1");

        if (cmd.hasOption("topics")) {
            tccConfiguration.setProperty("tx_consistent_consumer_topics_list", cmd.getOptionValue("topics"));
        }
        if (cmd.hasOption("max")) {
            tccConfiguration.setProperty("tx_consistent_consumer_max_number_of_kafka_consumers", cmd.getOptionValue("max"));
        }

        return new TxConsistentConsumerConfiguration(tccConfiguration);
    }

    private static void configureRootLogger(CommandLine cmd) {
        if (cmd.hasOption("loglevel")) {
            switch (cmd.getOptionValue("loglevel")) {
                case "ALL":
                    Logger.getRootLogger().setLevel(Level.ALL);
                    break;
                case "TRACE":
                    Logger.getRootLogger().setLevel(Level.TRACE);
                    break;
                case "DEBUG":
                    Logger.getRootLogger().setLevel(Level.DEBUG);
                    break;
                case "INFO":
                    Logger.getRootLogger().setLevel(Level.INFO);
                    break;
                case "WARN":
                    Logger.getRootLogger().setLevel(Level.WARN);
                    break;
                case "ERROR":
                    Logger.getRootLogger().setLevel(Level.ERROR);
                    break;
                case "FATAL":
                    Logger.getRootLogger().setLevel(Level.FATAL);
                    break;
                case "OFF":
                    Logger.getRootLogger().setLevel(Level.OFF);
                    break;
                default:
                    Logger.getRootLogger().setLevel(Level.ERROR);
            }
        } else {
            Logger.getRootLogger().setLevel(Level.ERROR);
        }
    }

    private static CommandLine parseCommandLine(String[] args, Options options) {
        CommandLineParser parser = new BasicParser();
        HelpFormatter helpFormatter = new HelpFormatter();
        helpFormatter.setWidth(200);
        CommandLine cmd;

        try {
            cmd = parser.parse(options, args);
            if (cmd.hasOption("help")) {
                helpFormatter.printHelp("java -cp \"lib/*\" com.helvetia.tcc.rest.AvroRest ", options);
                System.exit(0);
                return null;
            }
        } catch (ParseException e) {
            System.out.println(e.getMessage());
            helpFormatter.printHelp("java -cp \"lib/*\" com.helvetia.tcc.rest.AvroRest ", options);
            System.exit(1);
            return null;
        }
        return cmd;
    }

    private static Options buildCommandLineOptions() {
        Options options = new Options();

        Option instanceName = new Option("i", "instance-name", true, "CDC instance name");
        instanceName.setRequired(true);
        options.addOption(instanceName);

        Option subName = new Option("s", "sub-name", true, "Subscription name");
        subName.setRequired(true);
        options.addOption(subName);

        Option consumerPropertiesFilePath = new Option("c", "consumer-config", true,
                "Fully qualified consumer configuration file name");
        consumerPropertiesFilePath.setRequired(true);
        options.addOption(consumerPropertiesFilePath);

        Option maxNumberOfConsumers = new Option("n", "max", true,
                "Max number of Kafka consumers Transactionally consistent consumer allowed to use");
        maxNumberOfConsumers.setRequired(false);
        options.addOption(maxNumberOfConsumers);

        Option dumpRecords = new Option("d", "dump", false,
                "Dump all records into console (if not specified, print out statistics only)");
        dumpRecords.setRequired(false);
        options.addOption(dumpRecords);

        // TODO: Define what is meant with "wait"
        Option numberOfWaits = new Option("w", "waits", true,
                "Number of waits for data to be returned by Transactionally consistent consumer poll request");
        numberOfWaits.setRequired(false);
        numberOfWaits.setType(Number.class);
        options.addOption(numberOfWaits);

        Option startBookmarkOpt = new Option("b", "bookmark", true,
                "Start bookmark ID or HEAD or TAIL");
        startBookmarkOpt.setRequired(false);
        options.addOption(startBookmarkOpt);

        options.addOption("h", "help", false, "Show help");

        Option topicsList = new Option("t", "topics", true, "Topics List");
        topicsList.setRequired(false);
        options.addOption(topicsList);

        Option file = new Option("f", "file", true,
                "Write transactions as JSONs into file, do not call REST service");
        file.setRequired(false);
        options.addOption(file);

        Option loglevel = new Option("l", "loglevel", true,
                "Set log4j loglevel to \"ALL\", \"TRACE\", \"DEBUG\", \"INFO\"," +
                        " \"WARN\", \"ERROR\", \"FATAL\", \"OFF\"");
        loglevel.setRequired(false);
        options.addOption(loglevel);
        return options;
    }
}
