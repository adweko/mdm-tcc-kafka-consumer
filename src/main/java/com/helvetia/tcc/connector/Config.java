package com.helvetia.tcc.connector;

public class Config {

    public static class CVBoost {
        public static final String CVBOOST_WORKORDER_URL = "cvboost.workorder.url";
        public static final String CVBOOST_WORKORDER_FILE = "cvboost.workorder.file";
        public static final String CVBOOST_USERNAME = "cvboost.username";
        public static final String CVBOOST_PASSWORD = "cvboost.password";
    }

    public static class IBM {

    }

}
