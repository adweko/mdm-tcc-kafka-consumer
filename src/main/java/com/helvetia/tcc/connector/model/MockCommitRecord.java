package com.helvetia.tcc.connector.model;

import com.datamirror.ts.kafka.txconsistentconsumer.TopicRecordIf;
import com.datamirror.ts.kafka.txconsistentconsumer.TopicRecordType;
import com.datamirror.ts.kafka.txconsistentconsumer.TxConsistentConsumerException;
import com.datamirror.ts.kafka.txconsistentconsumer.bookmark.Bookmark;
import org.apache.kafka.clients.consumer.ConsumerRecord;

public class MockCommitRecord implements TopicRecordIf<byte[], byte[]> {
    @Override
    public ConsumerRecord<byte[], byte[]> getConsumerRecord() {
        return null;
    }

    // Note: This always returns the same value. Needs to be changed when any logic depends on that, currently it
    // does not
    @Override
    public Bookmark getBookmark() {
        try {
            return Bookmark.readExternal("000100244344432D4B41464B41494E5447312D5A4551324B41464B2D636F6D6D697473747265616D00000000001AC10E00000000");
        } catch (TxConsistentConsumerException e) {
            throw new RuntimeException("e");
        }
    }

    @Override
    public TopicRecordType getRecordType() {
        return TopicRecordType.COMMIT;
    }
}
