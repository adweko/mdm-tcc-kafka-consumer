package com.helvetia.tcc.connector.model;

import com.datamirror.ts.kafka.txconsistentconsumer.TopicRecordIf;
import com.datamirror.ts.kafka.txconsistentconsumer.TopicRecordType;
import com.datamirror.ts.kafka.txconsistentconsumer.TxConsistentConsumerException;
import com.datamirror.ts.kafka.txconsistentconsumer.bookmark.Bookmark;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.common.serialization.StringSerializer;

/**
 * We would like to be able to feed these into RecordHandler for tests.
 * Read them using StringDeserializers for key and value.
 */
public class ZepasTopicRecord implements TopicRecordIf<byte[], byte[]> {

    private final ZepasDataRecord zepasDataRecord;

    public ZepasTopicRecord(ZepasDataRecord zepasDataRecord) {
        this.zepasDataRecord = zepasDataRecord;
    }

    @Override
    public ConsumerRecord<byte[], byte[]> getConsumerRecord() {
        ConsumerRecord<byte[], byte[]> consumerRecord;
        try {
            ObjectMapper mapper = new ObjectMapper();
            StringSerializer serializer = new StringSerializer();
            String keyString = mapper.writeValueAsString(zepasDataRecord.getKey());
            String valueString = mapper.writeValueAsString(zepasDataRecord.getValue());

            consumerRecord = new ConsumerRecord<>(zepasDataRecord.getTopic(),
                    zepasDataRecord.getPartition(),
                    zepasDataRecord.getOffset(),
                    serializer.serialize("dummy", keyString),
                    serializer.serialize("dummy", valueString)
            );
            return consumerRecord;
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }



    @Override
    public Bookmark getBookmark() {
        try {
            return Bookmark.readExternal(zepasDataRecord.getBookmark());
        } catch (TxConsistentConsumerException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public TopicRecordType getRecordType() {
        return TopicRecordType.DML;
    }
}
