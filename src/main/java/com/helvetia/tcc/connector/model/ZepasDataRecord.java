package com.helvetia.tcc.connector.model;

import com.datamirror.ts.kafka.txconsistentconsumer.TopicRecordIf;
import com.datamirror.ts.kafka.txconsistentconsumer.bookmark.Bookmark;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.helvetia.tcc.connector.MDMTCCKafkaConsumer;
import com.helvetia.tcc.connector.exceptions.ZEPASDataQualityException;
import com.helvetia.tcc.connector.exceptions.ZepasRecordNotParseableException;
import com.jsoniter.JsonIterator;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.common.header.Headers;
import org.apache.kafka.common.record.TimestampType;
import org.apache.kafka.common.serialization.Deserializer;
import org.apache.log4j.Logger;
import org.apache.log4j.MDC;

import java.io.IOException;
import java.util.Map;
import java.util.Optional;


public class ZepasDataRecord {

    private static final Logger log = Logger.getLogger(MDMTCCKafkaConsumer.class.getName());
    public static final String KUPE_LAUFNUMMER = "KUPE_LAUFNUMMER";
    public static final String KUPE_LAUFNR_DEL = "KUPE_LAUFNR_DEL";
    public static final String KUKO_KUPE_LAUFNUMMER = "KUKO_KUPE_LAUFNUMMER";

    private String bookmark;
    //TODO: custom deserializer would be better, but this is not strictly needed for test deserialization since we have
    // bookmark as a field
    @JsonIgnore
    private Bookmark commitStream;
    private Map key;
    private Map value;
    private long timestamp;
    private TimestampType timestampType;
    private String topic;
    // TODO: custom deserializer would be better, but headers are currently empty anyways
    @JsonIgnore
    private Headers headers;
    private int partition;
    private long offset;
    private Integer kupeLaufnummer;

    @Override
    public String toString() {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            return objectMapper.writeValueAsString(this);
        } catch (JsonProcessingException e) {
            throw new RuntimeException("serializing ZepasDataRecord toString failed", e);
        }
    }

    public ZepasDataRecord() {
    }

    /**
     * Deserialize a record received from ZEPAS.
     * It is assumed that all records contain a kupeLaufnummer in either the key or value obtainable via any of the keys
     * KUPE_LAUFNUMMER, KUPE_LAUFNR_DEL or KUKO_KUPE_LAUFNUMMER.
     * For more info see {@link ZEPASDataQualityException}.
     *
     * @param tccRecord          record obtained from polling via tcc. It is assumed that tccRecord.getConsumerRecord().value() != null
     * @param keyDeserializer    deserializer for record key
     * @param valueDeserializer  deserializer for record value
     * @param lastKupeLaufNummer
     * @throws ZEPASDataQualityException when no kupeLaufnummer is present
     */
    public ZepasDataRecord(TopicRecordIf<byte[], byte[]> tccRecord, Deserializer<?> keyDeserializer,
                           Deserializer<?> valueDeserializer, final int lastKupeLaufNummer) throws ZEPASDataQualityException {
        bookmark = tccRecord.getBookmark().get();
        commitStream = tccRecord.getBookmark();
        ConsumerRecord<byte[], byte[]> consumerRecord = tccRecord.getConsumerRecord();
        timestamp = consumerRecord.timestamp();
        timestampType = consumerRecord.timestampType();
        topic = consumerRecord.topic();
        headers = consumerRecord.headers();
        partition = consumerRecord.partition();
        offset = consumerRecord.offset();
        JsonIterator keyJiterator = JsonIterator.parse(keyDeserializer.deserialize("key", consumerRecord.key()).toString());
        JsonIterator valueJiterator = JsonIterator.parse(valueDeserializer.deserialize("value", consumerRecord.value()).toString());
        try {
            key = keyJiterator.read(Map.class);
            value = valueJiterator.read(Map.class);
            kupeLaufnummer = extractKupeLaufnummerFromKeyOrValue(key, value);
        } catch (IOException e) {
            throw new ZepasRecordNotParseableException(e);
        }

    }

    private Integer extractKupeLaufnummerFromKeyOrValue(Map theKey, Map theValue) throws ZEPASDataQualityException {
        Optional<Integer> maybeKeyKupe = extractKupeLaufnummerFromMap(theKey);
        if (maybeKeyKupe.isPresent()) {
            log.info("Übernehme KUPE_Laufnummer aus Key: " + maybeKeyKupe.get());
            return maybeKeyKupe.get();
        }
        Optional<Integer> maybeValueKupe = extractKupeLaufnummerFromMap(theValue);
        if (maybeValueKupe.isPresent()) {
            log.info("Übernehme KUPE_Laufnummer aus Value: " + maybeValueKupe.get());
            return maybeValueKupe.get();
        }
        throw new ZEPASDataQualityException("No kupeLaufnummer in record from topic " + topic + " partition " +
                partition + " offset " + offset);
    }


    private Optional<Integer> extractKupeLaufnummerFromMap(Map map) {
        Object localKupeLaufNummer = null;
        String keyFound = "";
        if (map.containsKey(KUPE_LAUFNUMMER)) {
            localKupeLaufNummer = map.get(KUPE_LAUFNUMMER);
            keyFound = KUPE_LAUFNUMMER;
        } else if (map.containsKey(KUPE_LAUFNR_DEL)) {
            localKupeLaufNummer = map.get(KUPE_LAUFNR_DEL);
            keyFound = KUPE_LAUFNR_DEL;
        } else if (map.containsKey(KUKO_KUPE_LAUFNUMMER)) {
            // result from https://jira.helvetia.group/browse/MFNLT-13087
            // dealing with tkukb_koelverwbez
            localKupeLaufNummer = map.get(KUKO_KUPE_LAUFNUMMER);
            keyFound = KUKO_KUPE_LAUFNUMMER;
        }

        if (localKupeLaufNummer != null) {
            String stringKupe = localKupeLaufNummer.toString().trim();
            if (!stringKupe.isEmpty()) {
                return Optional.of(Integer.parseInt(stringKupe));
            } else {
                log.warn("Could not extract kupeLaufnummer even though " + keyFound + " was present in map, but it was empty");
            }
        }

        return Optional.empty();

    }


    public String getBookmark() {
        return bookmark;
    }

    public void setBookmark(String bookmark) {
        this.bookmark = bookmark;
    }

    public Bookmark getCommitStream() {
        return commitStream;
    }

    public void setCommitStream(Bookmark commitStream) {
        this.commitStream = commitStream;
    }

    public Map getKey() {
        return key;
    }

    public void setKey(Map key) {
        this.key = key;
    }

    public Map getValue() {
        return value;
    }

    public void setValue(Map value) {
        this.value = value;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public TimestampType getTimestampType() {
        return timestampType;
    }

    public void setTimestampType(TimestampType timestampType) {
        this.timestampType = timestampType;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public Headers getHeaders() {
        return headers;
    }

    public void setHeaders(Headers headers) {
        this.headers = headers;
    }

    public int getPartition() {
        return partition;
    }

    public void setPartition(int partition) {
        this.partition = partition;
    }

    public long getOffset() {
        return offset;
    }

    public void setOffset(long offset) {
        this.offset = offset;
    }

    public int getKupeLaufnummer() {
        return kupeLaufnummer;
    }

    public void setKupeLaufnummer(int kupeLaufnummer) {
        this.kupeLaufnummer = kupeLaufnummer;
    }
}
