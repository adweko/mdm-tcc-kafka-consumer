package com.helvetia.tcc.connector.model;

/**
 * Singleton that counts number of operations performed with TCC
 */
public class OperationCounter {

    private static OperationCounter instance = null;

    private int numberOfDmlOps = 0;
    private int numberOfCommitOps = 0;


    public static OperationCounter getInstance() {
        if (instance == null) {
            instance = new OperationCounter();
        }
        return instance;
    }

    public void increaseNumberOfDmlOpsByOne() {
        numberOfDmlOps += 1;
    }

    public void increaseNumberOfCommitOpsByOne() {
        numberOfCommitOps += 1;
    }

    public int getNumberOfDmlOps() {
        return numberOfDmlOps;
    }

    public int getNumberOfCommitOps() {
        return numberOfCommitOps;
    }
}
