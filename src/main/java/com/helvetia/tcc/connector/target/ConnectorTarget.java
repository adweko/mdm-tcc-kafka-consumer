package com.helvetia.tcc.connector.target;

/**
 * Interface for the target messages are sent to, as in CVBoost Webservice, local file, Kafka, etc.
 */
public interface ConnectorTarget {
    void initialize();
    void send(String message, int currentKupeLaufnummer);
}
