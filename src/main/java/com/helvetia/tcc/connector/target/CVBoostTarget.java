package com.helvetia.tcc.connector.target;

import com.helvetia.tcc.connector.exceptions.SendToTargetFailedException;
import com.helvetia.tcc.connector.MDMTCCKafkaConsumer;
import com.helvetia.tcc.response.CvboostResponse;
import org.apache.log4j.Logger;
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;
import org.glassfish.jersey.media.multipart.FormDataMultiPart;
import org.glassfish.jersey.media.multipart.MultiPart;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.media.multipart.file.FileDataBodyPart;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import java.io.File;
import java.io.FileWriter;

public class CVBoostTarget implements ConnectorTarget {

    private static final Logger log = Logger.getLogger(MDMTCCKafkaConsumer.class.getName());

    private HttpAuthenticationFeature httpAuthenticationFeature;
    private Client client;
    private WebTarget webTarget;
    private final String cvboostWorkOrderUrl;
    private final String cvboostWorkOrderFile;
    private final String cvboostUserName;
    private final String cvboostPassWord;

    public CVBoostTarget(String cvboostWorkOrderUrl, String cvboostWorkOrderFile, String cvboostUserName, String cvboostPassWord) {
        this.cvboostWorkOrderUrl = cvboostWorkOrderUrl;
        this.cvboostWorkOrderFile = cvboostWorkOrderFile;
        this.cvboostUserName = cvboostUserName;
        this.cvboostPassWord = cvboostPassWord;
    }

    @Override
    public void initialize() {
        httpAuthenticationFeature = HttpAuthenticationFeature.basic(this.cvboostUserName, this.cvboostPassWord);
        client = ClientBuilder.newBuilder()
                .register(httpAuthenticationFeature)
                .register(MultiPartFeature.class)
                .build();
        webTarget = client.target(this.cvboostWorkOrderUrl);
    }

    @Override
    public void send(String message, int currentKupeLaufNummer) {
        try {
            try (FileWriter fileWriter = new FileWriter(this.cvboostWorkOrderFile)) {
                fileWriter.write(message);
                fileWriter.flush();
            }
            // create file object to Json text file, transaction.json is the file that will be processed
            // in the cvboost workorder and contains the TCC transaction data in JSON format
            File file = new File(this.cvboostWorkOrderFile);
            // attach text file to REST call
            // has to be called workorder.file so it can be handled in cvboost workorder file resource context
            FileDataBodyPart filePart = new FileDataBodyPart("workorder.file", file);
            MultiPart multipartEntity = new FormDataMultiPart()
                    .bodyPart(filePart);
            // call REST service
            log.debug("Calling CVBoost REST service");
            Response response = webTarget.request().post(
                    Entity.entity(multipartEntity, multipartEntity.getMediaType()));
            log.debug("Call ended");
            if (response.getStatus() == 200) {
                CvboostResponse cvboostResponse = response.readEntity(CvboostResponse.class);
                // Log Bookmark and attach Workorder ID
                log.info("Workorder ID: " + cvboostResponse.getId()
                        + " Status: " + cvboostResponse.getStatus()
                        + " Name: " + cvboostResponse.getName()
                        + " KUPE_LAUFNUMMER: " + currentKupeLaufNummer);
            } else {
                log.error("Error calling CVBoost REST service: " + response);
                throw new SendToTargetFailedException("Failed calling CVBoost WS, response is " + response);
            }
            response.close();
        } catch (Exception e) {
            throw new SendToTargetFailedException(e);
        }
    }
}
