package com.helvetia.tcc.connector.target;

import com.helvetia.tcc.connector.MDMTCCKafkaConsumer;
import org.apache.log4j.Logger;

import java.util.ArrayList;

public class MockTarget implements ConnectorTarget {

    private ArrayList<String> stringsWritten = new ArrayList<>();
    private static final Logger log = Logger.getLogger(MDMTCCKafkaConsumer.class.getName());

    @Override
    public void initialize() {
        stringsWritten = new ArrayList<>();
    }

    @Override
    public void send(String message, int currentKupeLaufnummer) {
        log.info("Sending message to MockTarget");
        stringsWritten.add(message);
    }

    public ArrayList<String> getStringsWritten() {
        return stringsWritten;
    }
}
