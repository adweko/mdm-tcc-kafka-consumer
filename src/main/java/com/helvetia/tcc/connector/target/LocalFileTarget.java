package com.helvetia.tcc.connector.target;

import com.helvetia.tcc.connector.exceptions.SendToTargetFailedException;
import com.helvetia.tcc.connector.MDMTCCKafkaConsumer;
import org.apache.log4j.Logger;

import java.io.FileWriter;
import java.io.IOException;

public class LocalFileTarget implements ConnectorTarget {

    private static final Logger log = Logger.getLogger(MDMTCCKafkaConsumer.class.getName());
    private final String CONSOLE_TRANSACTION_DUMP_FILE_NAME;

    public LocalFileTarget(String CONSOLE_TRANSACTION_DUMP_FILE_NAME) {
        this.CONSOLE_TRANSACTION_DUMP_FILE_NAME = CONSOLE_TRANSACTION_DUMP_FILE_NAME;
    }

    @Override
    public void initialize() {

    }

    @Override
    public void send(String message, int currentKupeLaufnummer) {
        // write transaction data to file system only
        log.info("No CVBoost REST service call, writing file only");
        try (FileWriter writer = new FileWriter(CONSOLE_TRANSACTION_DUMP_FILE_NAME)) {
            writer.write(message);
            writer.write("\n{\"End of transaction\": true}\n");
            writer.flush();
        } catch (IOException e) {
            throw new SendToTargetFailedException(e);
        }
    }

}
