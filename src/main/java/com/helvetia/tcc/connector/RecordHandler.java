package com.helvetia.tcc.connector;

import com.datamirror.ts.kafka.txconsistentconsumer.TopicRecordIf;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.helvetia.tcc.connector.checkpoint.BookmarkCheckpoint;
import com.helvetia.tcc.connector.exceptions.CommitFailedException;
import com.helvetia.tcc.connector.exceptions.ZEPASDataQualityException;
import com.helvetia.tcc.connector.model.OperationCounter;
import com.helvetia.tcc.connector.model.ZepasDataRecord;
import com.helvetia.tcc.connector.target.ConnectorTarget;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.common.serialization.Deserializer;
import org.apache.log4j.Logger;
import org.apache.log4j.MDC;

import java.util.ArrayList;
import java.util.List;

public class RecordHandler {

    private String lastCommittedBookmark;
    private String bookmarkInProcess;
    private final ArrayList<ZepasDataRecord> transactionData = new ArrayList<>();
    private int lastSeenKupeLaufNummer = 0;

    private static final Logger log = Logger.getLogger(MDMTCCKafkaConsumer.class.getName());

    private final BookmarkCheckpoint bookMarkCheckpoint;
    private final Deserializer<?> keyDeserializer;
    private final Deserializer<?> valueDeserializer;
    private final boolean logTransactionsToStdOut;

    private final ConnectorTarget target;

    public RecordHandler(ConnectorTarget connectorTarget,
                         BookmarkCheckpoint bookMarkCheckpoint,
                         Deserializer<?> keyDeserializer,
                         Deserializer<?> valueDeserializer,
                         boolean logTransactionsToStdOut) {
        this.target = connectorTarget;
        this.bookMarkCheckpoint = bookMarkCheckpoint;
        this.keyDeserializer = keyDeserializer;
        this.valueDeserializer = valueDeserializer;
        this.logTransactionsToStdOut = logTransactionsToStdOut;

        this.lastCommittedBookmark = bookMarkCheckpoint.getLatest();
    }

    public void handleRecords(List<TopicRecordIf<byte[], byte[]>> records) {
        log.info("-- New bundle of records --");
        target.initialize();
        records.forEach(this::handleRecord);
        MDC.clear();
    }

    private void handleRecord(TopicRecordIf<byte[], byte[]> record) {
        log.info("--- New record ---");
        String bookMarkOfRecord = record.getBookmark().get();
        log.info("Bookmark: " + bookMarkOfRecord);
        if (!bookMarkOfRecord.equals(lastCommittedBookmark)) {
            bookmarkInProcess = bookMarkOfRecord;
            handleRecordByType(record);
        } else {
            log.info("Skipping record, as its bookmark " + bookMarkOfRecord + " equals the lastCommittedBookmark " + lastCommittedBookmark);
        }
    }

    private void handleRecordByType(TopicRecordIf<byte[], byte[]> record) throws CommitFailedException {
        switch (record.getRecordType()) {
            case DML:
                log.info("DML record in process.");
                handleDataMessage(record);
                break;
            case COMMIT:
                log.info("COMMIT record in process.");
                handleCommitMessage(record);
                break;
            default:
                log.warn("Unknown record type. No processing");
                break;
        }
    }

    private void handleDataMessage(TopicRecordIf<byte[], byte[]> record) {
        ConsumerRecord<byte[], byte[]> consumerRecord = record
                .getConsumerRecord();
        log.info("Topic: " + consumerRecord.topic() + " Partition: " + consumerRecord.partition() +
                " Offset: " + consumerRecord.offset());

        if (consumerRecord.value() == null) {
            log.info("record value is NULL. record is skipped");
            OperationCounter.getInstance().increaseNumberOfDmlOpsByOne();
            return;
        }

        ZepasDataRecord zepasDataRecord = null;
        try {
            zepasDataRecord = new ZepasDataRecord(record, keyDeserializer, valueDeserializer, lastSeenKupeLaufNummer);
        } catch (ZEPASDataQualityException e) {
            log.error("record from topic " + consumerRecord.topic() + " partition " + consumerRecord.partition()
                    + " offset " + consumerRecord.offset() + " does not contain a kupeLaufnummer. Skipping record," +
                    " further analysis and correction in ZEPAS required.");
        }

        // START Implementation MF@NL TransMFNLT-6942
        // Filter out if the the record is from table TKUPI_PEZUSATZINFO
        // and has INFOARTCODE != 102
        if (zepasDataRecord.getTopic().endsWith("tkupi_pezusatzinfo")) {
            if (zepasDataRecord.getValue() != null && zepasDataRecord.getValue().containsKey("INFOARTCODE")) {
                String infoArtCode = zepasDataRecord.getValue().get("INFOARTCODE").toString();
                if (!infoArtCode.equals("102")) {
                    log.info("Ignoring irrelevant record from topic " +
                            consumerRecord.topic() +
                            " partition " + consumerRecord.partition() +
                            " offset " + consumerRecord.offset() +
                            " since INFOARTCODE is " + infoArtCode);
                    OperationCounter.getInstance().increaseNumberOfDmlOpsByOne();
                    return;
                }
            }
        }
        // END Implementation MF@NL TransMFNLT-6942


        if (lastSeenKupeLaufNummer == 0) {
            lastSeenKupeLaufNummer = zepasDataRecord.getKupeLaufnummer();
        }

        // kupeLaufnummer change between messages
        if ((zepasDataRecord.getKupeLaufnummer() != lastSeenKupeLaufNummer) &&
                !transactionData.isEmpty() &&
                lastSeenKupeLaufNummer != 0) {
            log.info("KUPE_LAUFNUMMER changed from " + lastSeenKupeLaufNummer + " to: "
                    + zepasDataRecord.getKupeLaufnummer() + ". Closing old transaction.");
            closeCurrentTransaction();
        }

        transactionData.add(zepasDataRecord);
        lastSeenKupeLaufNummer = zepasDataRecord.getKupeLaufnummer();

        log.info("Record with Topic " + consumerRecord.topic() +
                " Partition " + consumerRecord.partition() +
                " Offset " + consumerRecord.offset() +
                " added to transaction.");

        OperationCounter.getInstance().increaseNumberOfDmlOpsByOne();
    }

    private void handleCommitMessage(TopicRecordIf<byte[], byte[]> record) {
        log.info("Closing transaction because of Commit record.");
        closeCurrentTransaction();
        OperationCounter.getInstance().increaseNumberOfCommitOpsByOne();
    }

    private void closeCurrentTransaction() {
        if (!transactionData.isEmpty()) {
            sendTransaction();
            commitCurrentBookmark();
            transactionData.clear();
            log.info("---- Transaction closed ----");
        } else {
            log.info("---- Transaction closed, but it was empty ----");
        }
    }


    private void commitCurrentBookmark() {
        bookMarkCheckpoint.commit(bookmarkInProcess);
        lastCommittedBookmark = bookmarkInProcess;
        log.info("lastCommittedbookmark = " + lastCommittedBookmark);
    }

    private void sendTransaction() {
        Gson prettyGson = new GsonBuilder().setPrettyPrinting().create();
        String transactionJson = prettyGson.toJson(transactionData);
        if (logTransactionsToStdOut) {
            System.out.println(transactionData);
        }
        target.send(transactionJson, lastSeenKupeLaufNummer);
    }

}
