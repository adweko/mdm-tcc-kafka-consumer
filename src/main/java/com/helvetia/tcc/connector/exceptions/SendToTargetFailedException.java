package com.helvetia.tcc.connector.exceptions;

/**
 * Inidcated that web service call failed
 */
public class SendToTargetFailedException extends ConnectorRuntimeException {
    public SendToTargetFailedException() {
    }

    public SendToTargetFailedException(String message) {
        super(message);
    }

    public SendToTargetFailedException(String message, Throwable cause) {
        super(message, cause);
    }

    public SendToTargetFailedException(Throwable cause) {
        super(cause);
    }

    public SendToTargetFailedException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
