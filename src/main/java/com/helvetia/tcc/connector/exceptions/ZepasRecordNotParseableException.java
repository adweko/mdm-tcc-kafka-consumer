package com.helvetia.tcc.connector.exceptions;

/**
 * The key or value of a ZEPAS Kafka message were not parseable
 */
public class ZepasRecordNotParseableException extends ConnectorRuntimeException {
    public ZepasRecordNotParseableException() {
    }

    public ZepasRecordNotParseableException(String message) {
        super(message);
    }

    public ZepasRecordNotParseableException(String message, Throwable cause) {
        super(message, cause);
    }

    public ZepasRecordNotParseableException(Throwable cause) {
        super(cause);
    }

    public ZepasRecordNotParseableException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }


}