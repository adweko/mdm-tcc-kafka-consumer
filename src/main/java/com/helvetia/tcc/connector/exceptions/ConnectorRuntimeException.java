package com.helvetia.tcc.connector.exceptions;

public class ConnectorRuntimeException extends RuntimeException {
    public ConnectorRuntimeException() {
    }

    public ConnectorRuntimeException(String message) {
        super(message);
    }

    public ConnectorRuntimeException(String message, Throwable cause) {
        super(message, cause);
    }

    public ConnectorRuntimeException(Throwable cause) {
        super(cause);
    }

    public ConnectorRuntimeException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
