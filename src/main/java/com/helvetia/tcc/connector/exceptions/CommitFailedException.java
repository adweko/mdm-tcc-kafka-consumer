package com.helvetia.tcc.connector.exceptions;

/**
 * Indicates that commiting the last bookmark failed, due to failure to write to file
 */
public class CommitFailedException extends ConnectorRuntimeException {

    public CommitFailedException(String message) {
        super(message);
    }

    public CommitFailedException(String message, Throwable cause) {
        super(message, cause);
    }

    public CommitFailedException(Throwable cause) {
        super(cause);
    }

    public CommitFailedException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public CommitFailedException() {
        super();
    }
}
