package com.helvetia.tcc.connector.exceptions;

/** Indicates that there is an inconsistency in the data received from ZEPAS.
 * This happens for example when the KUKO_KUPE_LAUFNUMMER in the table TKUKB_KOELVERWBEZ is empty.
 *
 * Information received concerning this from Andreas Fenske:
 *
 * select
 * cast( kuko.KUPE_LAUFNUMMER as char(11)) as KUKO_KUPE_LAUFNUMMER
 * FROM Z00.TKUKO_KOMMELEMENT kuko
 * WHERE 1=1
 * AND kuko.KUKO_MANDANT = <TKUKB_KOELVERWBEZ.KUKB_MANDANT>
 * AND kuko.KUKO_LAUFNUMMER = <TKUKB_KOELVERWBEZ.KUKO_LAUFNUMMER>
 * AND CURRENT DATE between kuko.KUKO_GUELTIGAB AND kuko.GUELTIGBIS
 * 11:28 Uhr
 * Das wäre die Query, ich selektiere nur eine Version.. es muss eigentlich immer eine gültige Version geben
 * 11:29 Uhr
 * Wenn es keinen Treffer gibt, ist die KUKO_KUPE_LAUFNUMMER dann leer
 * 11:30 Uhr
 * Das kann eben passieren, wenn es irgendeinen Datenfehler gibt.. das kann ich nicht ausschliessen, aber da müsste MDM evtl. einen Alert machen
 *
 */
public class ZEPASDataQualityException extends Exception {
    public ZEPASDataQualityException() {
        super();
    }

    public ZEPASDataQualityException(String message) {
        super(message);
    }

    public ZEPASDataQualityException(String message, Throwable cause) {
        super(message, cause);
    }

    public ZEPASDataQualityException(Throwable cause) {
        super(cause);
    }

    protected ZEPASDataQualityException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
