package com.helvetia.tcc.connector.helper;

import com.datamirror.ts.kafka.txconsistentconsumer.bookmark.BookmarkFactory;
import com.datamirror.ts.kafka.txconsistentconsumer.bookmark.TxConsistentConsumerBookmark;
import org.apache.commons.cli.*;
import org.apache.kafka.clients.consumer.*;
import org.apache.kafka.common.PartitionInfo;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.serialization.ByteArrayDeserializer;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Duration;
import java.time.Instant;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.stream.Collectors;

/**
 * Obtain, for a certain point in time, the latest Bookmark from the tcc CommitStream-Topic.
 */
public class BookmarkForTimestamp {

    public static final String TIMESTAMP_OPT_LONG = "timestamp";
    public static final String PROPERTIES_OPT_LONG = "properties";
    public static final String TOPIC_OPT_LONG = "topic";

    public static void main(String[] args) throws ParseException, IOException {

        CommandLine commandLine = parseCommandLine(args, buildCommandLineOptions());
        final Path confPath = Paths.get(commandLine.getOptionValue(PROPERTIES_OPT_LONG));
        final String topic = commandLine.getOptionValue(TOPIC_OPT_LONG);
        final String timestampString = commandLine.getOptionValue(TIMESTAMP_OPT_LONG);

        final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ISO_INSTANT;
        long timestampToSearchFor = Instant.from(dateTimeFormatter.parse(timestampString)).toEpochMilli();

        Properties consumerConfig = new Properties();
        consumerConfig.load(new FileInputStream(confPath.toFile()));

        consumerConfig.forEach((k, v) -> System.out.println(k + " " + v));
        consumerConfig.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, ByteArrayDeserializer.class);
        consumerConfig.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, ByteArrayDeserializer.class);
        Consumer<byte[], byte[]> consumer = new KafkaConsumer<>(consumerConfig);

        final List<PartitionInfo> partitionInfos = consumer.partitionsFor(topic);
        final List<TopicPartition> topicPartitions = partitionInfos.stream()
                .map(info -> new TopicPartition(info.topic(), info.partition()))
                .collect(Collectors.toList());
        consumer.assign(topicPartitions);
        Map<TopicPartition, Long> endOffsets = consumer.endOffsets(consumer.assignment());

        consumer.assign(endOffsets.keySet());


        final Map<TopicPartition, Long> topicPartitionToDesiredTimestamp = endOffsets.keySet().stream()
                .collect(Collectors.toMap(topicPartition -> topicPartition, topicPartition -> timestampToSearchFor));
        final Map<TopicPartition, OffsetAndTimestamp> topicPartitionToDesiredOffsetAndTimestamp = consumer.offsetsForTimes(topicPartitionToDesiredTimestamp);

        // we go exactly one message back from the offset
        // offsetForTimes from the Consumer gives us the next possible offset after timestamp
        // however, since we want to use the bookmark as "lastCommittedBookmark", we need one before that
        topicPartitionToDesiredOffsetAndTimestamp.forEach((tp, oat) -> consumer.seek(tp, oat.offset() - 1));

        final int maxMessagesPrinted = 1;
        int messagesPrinted = 0;
        for (ConsumerRecord<byte[], byte[]> record : consumer.poll(Duration.ofSeconds(1L))) {
            if (messagesPrinted < maxMessagesPrinted) {
                // this creates the Bookmark for this commitstreamtopic at this offset (corresponding to timestamp)
                // currentEntrySeqNumber is set to -1 (which is operationSequence in Bookmark). This should be
                // "start of this transaction", which would be consistent to TxConsistentConsumerCommitStreamBookmarkBuilder's
                // implementation of HEAD and TAIL
                TxConsistentConsumerBookmark txConsistentConsumerBookmark = BookmarkFactory.create(record.topic(), record.offset(), -1);
                System.out.println("timestamp: " + record.timestamp() + " (" + Instant.ofEpochMilli(record.timestamp()) + ") corresponds to Bookmark " + txConsistentConsumerBookmark);
                messagesPrinted++;
            }
        }


    }


    private static CommandLine parseCommandLine(String[] args, Options options) throws ParseException {
        CommandLineParser parser = new BasicParser();
        HelpFormatter helpFormatter = new HelpFormatter();
        helpFormatter.setWidth(200);
        CommandLine cmd;
        cmd = parser.parse(options, args);
        return cmd;
    }

    private static Options buildCommandLineOptions() {
        Options options = new Options();

        Option timestamp = new Option("t", TIMESTAMP_OPT_LONG, true,
                "ISO_INSTANT Timestamp (UTC) for which to obtain the corresponding Bookmark from Commitstream topic. See " +
                        "https://docs.oracle.com/javase/8/docs/api/java/time/format/DateTimeFormatter.html#ofPattern-java.lang.String-java.util.Locale- " +
                        "and note that decimal places below seconds are resolved up to nanosecond, but later converted toEpochMilli() for Kafka. " +
                        "You will probably want to use something like \"2020-08-10T15:20:04.073Z\"");
        timestamp.setRequired(true);
        options.addOption(timestamp);

        Option pathToConfig = new Option("p", PROPERTIES_OPT_LONG, true,
                "Absolute path to the properties file of the tcc. Only need Kafka-related properties from there.");
        pathToConfig.setRequired(true);
        options.addOption(pathToConfig);

        Option commitStreamTopicName = new Option("t", TOPIC_OPT_LONG, true,
                "Name of the CommitStream topic.");
        commitStreamTopicName.setRequired(true);
        options.addOption(commitStreamTopicName);

        return options;
    }
}
