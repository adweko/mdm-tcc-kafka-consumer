# Obtain the Bookmark for a certain timestamp from the commitStream topic.

TIMESTAMP="2020-08-10T15:20:04.073Z"
TOPIC="CDC-KAFKATEST1-ZEQ2KAFK-commitstream"
PROPERTIES_FILE_PATH="../../resources/config.properties"


#export ROOTDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd | awk -F 'env' '{print $1}')"

PATH_TO_JAR=../../../../target/

java -cp ${PATH_TO_JAR}/*.jar \
 com.helvetia.tcc.connector.helper.BookmarkForTimestamp \
 --timestamp ${TIMESTAMP} \
 --properties ${PROPERTIES_FILE_PATH} \
 --topic ${TOPIC}