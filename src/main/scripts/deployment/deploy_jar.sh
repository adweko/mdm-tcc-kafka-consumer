usage() {
    [[ $1 ]] && echo "Error: $1"
    echo "usage: $0 -e <env-file>"
    exit 0
}
[[ $# -eq 0 ]] && usage

while getopts ":he:" arg; do
  case $arg in
    e) # Specify env file.
      export ENV_FILE=${OPTARG}
      ;;
    h | *) # Display usage and exit
      usage
      ;;
  esac
done

[[ -z ${ENV_FILE} ]] && usage "env file not set"
source "${ENV_FILE}"

ssh -p ${PORT} ${USER}@${SERVER} 'systemctl stop cvboostconnector.service;
  mkdir -p /opt/cvboostconnector/backup/
  cp /opt/cvboostconnector/MDMTCCKafkaConsumer.jar /opt/cvboostconnector/backup/MDMTCCKafkaConsumer.jar.`date -Ins`'

scp -P ${PORT} ${ROOTDIR}/target/MDMTCCKafkaConsumer-jar-with-dependencies.jar ${USER}@${SERVER}:/opt/cvboostconnector/

ssh -p ${PORT} ${USER}@${SERVER} "mv /opt/cvboostconnector/MDMTCCKafkaConsumer-jar-with-dependencies.jar /opt/cvboostconnector/MDMTCCKafkaConsumer.jar;
  chown ${SERVER_USER} /opt/cvboostconnector/MDMTCCKafkaConsumer.jar"

ssh -p ${PORT} ${USER}@${SERVER} 'systemctl start cvboostconnector.service'
