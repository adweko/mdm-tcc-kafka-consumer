package com.helvetia.tcc.connector;

import com.datamirror.ts.kafka.txconsistentconsumer.TopicRecordIf;
import com.datamirror.ts.kafka.txconsistentconsumer.TopicRecordType;
import com.datamirror.ts.kafka.txconsistentconsumer.TxConsistentConsumerException;
import com.datamirror.ts.kafka.txconsistentconsumer.bookmark.Bookmark;
import com.helvetia.tcc.connector.exceptions.ZEPASDataQualityException;
import com.helvetia.tcc.connector.model.ZepasDataRecord;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ZepasDataRecordTest {

    Serde<String> serde = Serdes.String();

    public ZepasDataRecordTest() throws TxConsistentConsumerException {
    }

    private static class TopicRecordWithBookmark<K, V> implements TopicRecordIf<K, V> {
        ConsumerRecord<K, V> record;
        Bookmark bookmark;
        TopicRecordType type;

        public TopicRecordWithBookmark(ConsumerRecord<K, V> record, Bookmark bookmark, TopicRecordType type) {
            this.record = record;
            this.bookmark = bookmark;
            this.type = type;
        }

        @Override
        public ConsumerRecord<K, V> getConsumerRecord() {
            return this.record;
        }

        @Override
        public Bookmark getBookmark() {
            return this.bookmark;
        }

        @Override
        public TopicRecordType getRecordType() {
            return this.type;
        }
    }

    private final Bookmark bookmark = Bookmark.readExternal("000100244344432D4B41464B41494E5447312D5A4551324B41464B2D636F6D6D697473747265616D00000000001AC10E00000001");

    @Test
    public void kupeLaufnummerIsTakenFromValueIfNotPresentInKey() throws ZEPASDataQualityException {
        String key = "{\n        \"KUKO_MANDANT\": 1,\n        \"KUKO_LAUFNUMMER\": 200000210,\n        \"KUKO_GUELTIGAB\": \"2020-06-12\",\n        \"KUKO_EINGABEAM\": \"2020-06-12T13:20:27.440965000000\"\n    },\n    \"schema\": {\n        \"type\": \"record\",\n        \"name\": \"TKUKO_KOMMELEMENT\",\n        \"namespace\": \"key.SOURCEDB.ZDKUB000.Z00\",\n        \"fields\": [\n           {\n                \"name\": \"KUKO_MANDANT\",\n                \"type\": {\n                    \"type\": \"int\",\n                    \"logicalType\": \"SMALLINT\",\n                    \"dbColumnName\": \"KUKO_MANDANT\"\n                },\n                \"default\": 0\n            },\n           {\n                \"name\": \"KUKO_LAUFNUMMER\",\n                \"type\": {\n                    \"type\": \"long\",\n                    \"logicalType\": \"DECIMAL\",\n                    \"dbColumnName\": \"KUKO_LAUFNUMMER\",\n                    \"precision\": 11,\n                    \"scale\": 0\n                },\n                \"default\": 0\n            },\n           {\n                \"name\": \"KUKO_GUELTIGAB\",\n                \"type\": {\n                    \"type\": \"string\",\n                    \"logicalType\": \"DATE\",\n                    \"dbColumnName\": \"KUKO_GUELTIGAB\",\n                    \"length\": 10\n                },\n                \"default\": \"\"\n            },\n           {\n                \"name\": \"KUKO_EINGABEAM\",\n                \"type\": {\n                    \"type\": \"string\",\n                    \"logicalType\": \"TIMESTAMP\",\n                    \"dbColumnName\": \"KUKO_EINGABEAM\",\n                    \"length\": 26\n                },\n                \"default\": \"\"\n            }\n        ]\n    }";
        String value = "{\n        \"KUKO_MANDANT\": 1,\n        \"KUKO_LAUFNUMMER\": 200000210,\n        \"KUKO_GUELTIGAB\": \"2020-06-12\",\n        \"KUKO_EINGABEAM\": \"2020-06-12T13:20:27.440965000000\",\n        \"GUELTIGBIS\": \"9999-12-31\",\n        \"KOMMELEMENTVALUE\": \"email@privat.ch                                             \",\n        \"KOMMENTAR\": \"                              \",\n        \"PRIORITAET\": 0,\n        \"HISTSTATUS\": \"1\",\n        \"EINGABESB\": \"CHTI-MDM\",\n        \"KUPE_LAUFNUMMER\": 85000903,\n        \"KUKT_LAUFNUMMER\": 130,\n        \"MC_TIMESTAMP\": \"2020-06-12-13.20.27.690716\",\n        \"SOURCE_SYSTEM\": \"CH_ZEPAS\"\n    }";
        ConsumerRecord<byte[], byte[]> consumerRecord = new ConsumerRecord<>("topic", 1, 1, serde.serializer().serialize("dummy", key), serde.serializer().serialize("dummy", value));
        TopicRecordWithBookmark<byte[], byte[]> topicRecordWithBookmark = new TopicRecordWithBookmark<>(consumerRecord, bookmark, TopicRecordType.DML);
        ZepasDataRecord zepasDataRecord = new ZepasDataRecord(topicRecordWithBookmark, serde.deserializer(), serde.deserializer(), 0);
        Assertions.assertEquals(85000903, zepasDataRecord.getKupeLaufnummer());
    }


    @Test
    public void noKupeInRecordLeadsToException() {
        String key = "{\n        \"KUKO_MANDANT\": 1,\n        \"KUKO_LAUFNUMMER\": 200000210,\n        \"KUKO_GUELTIGAB\": \"2020-06-12\",\n        \"KUKO_EINGABEAM\": \"2020-06-12T13:20:27.440965000000\"\n    },\n    \"schema\": {\n        \"type\": \"record\",\n        \"name\": \"TKUKO_KOMMELEMENT\",\n        \"namespace\": \"key.SOURCEDB.ZDKUB000.Z00\",\n        \"fields\": [\n           {\n                \"name\": \"KUKO_MANDANT\",\n                \"type\": {\n                    \"type\": \"int\",\n                    \"logicalType\": \"SMALLINT\",\n                    \"dbColumnName\": \"KUKO_MANDANT\"\n                },\n                \"default\": 0\n            },\n           {\n                \"name\": \"KUKO_LAUFNUMMER\",\n                \"type\": {\n                    \"type\": \"long\",\n                    \"logicalType\": \"DECIMAL\",\n                    \"dbColumnName\": \"KUKO_LAUFNUMMER\",\n                    \"precision\": 11,\n                    \"scale\": 0\n                },\n                \"default\": 0\n            },\n           {\n                \"name\": \"KUKO_GUELTIGAB\",\n                \"type\": {\n                    \"type\": \"string\",\n                    \"logicalType\": \"DATE\",\n                    \"dbColumnName\": \"KUKO_GUELTIGAB\",\n                    \"length\": 10\n                },\n                \"default\": \"\"\n            },\n           {\n                \"name\": \"KUKO_EINGABEAM\",\n                \"type\": {\n                    \"type\": \"string\",\n                    \"logicalType\": \"TIMESTAMP\",\n                    \"dbColumnName\": \"KUKO_EINGABEAM\",\n                    \"length\": 26\n                },\n                \"default\": \"\"\n            }\n        ]\n    }";
        String value = "{\n        \"KUKO_MANDANT\": 1,\n        \"KUKO_LAUFNUMMER\": 200000210,\n        \"KUKO_GUELTIGAB\": \"2020-06-12\",\n        \"KUKO_EINGABEAM\": \"2020-06-12T13:20:27.440965000000\",\n        \"GUELTIGBIS\": \"9999-12-31\",\n        \"KOMMELEMENTVALUE\": \"email@privat.ch                                             \",\n        \"KOMMENTAR\": \"                              \",\n        \"PRIORITAET\": 0,\n        \"HISTSTATUS\": \"1\",\n        \"EINGABESB\": \"CHTI-MDM\",\n        \"KUPE_LAUFNUMMER\": \"\",\n        \"KUKT_LAUFNUMMER\": 130,\n        \"MC_TIMESTAMP\": \"2020-06-12-13.20.27.690716\",\n        \"SOURCE_SYSTEM\": \"CH_ZEPAS\"\n    }";
        ConsumerRecord<byte[], byte[]> consumerRecord = new ConsumerRecord<>("topic", 1, 1, serde.serializer().serialize("dummy", key), serde.serializer().serialize("dummy", value));
        TopicRecordWithBookmark<byte[], byte[]> topicRecordWithBookmark = new TopicRecordWithBookmark<>(consumerRecord, bookmark, TopicRecordType.DML);
        Assertions.assertThrows(ZEPASDataQualityException.class, () ->  new ZepasDataRecord(topicRecordWithBookmark, serde.deserializer(), serde.deserializer(), 0));
    }


}
