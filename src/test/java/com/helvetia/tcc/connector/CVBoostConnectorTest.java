package com.helvetia.tcc.connector;


import com.datamirror.ts.kafka.txconsistentconsumer.TopicRecordIf;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.helvetia.tcc.connector.checkpoint.MockCheckpoint;
import com.helvetia.tcc.connector.model.MockCommitRecord;
import com.helvetia.tcc.connector.model.ZepasDataRecord;
import com.helvetia.tcc.connector.model.ZepasTopicRecord;
import com.helvetia.tcc.connector.target.MockTarget;
import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static org.mockito.Mockito.*;

class CVBoostConnectorTest {

    ObjectMapper objectMapper = new ObjectMapper();
    final Deserializer<String> stringDeserializer = new StringDeserializer();
    MockTarget target;
    RecordHandler handler;

    @BeforeEach
    public void setup() {
        target = spy(new MockTarget());
        handler = spy(new RecordHandler(
                target,
                new MockCheckpoint(),
                stringDeserializer,
                stringDeserializer,
                false
        ));
    }

    @Test
    public void testTransactionWithSameKuPeSendOnlyOnCommitMessage() throws IOException {
        List<TopicRecordIf<byte[], byte[]>> firstRecord = readSingleZepasRecordFrom("src/test/resources/record1_kupe_123.json");
        List<TopicRecordIf<byte[], byte[]>> secondRecord = readSingleZepasRecordFrom("src/test/resources/record2_kupe_123.json");
        List<TopicRecordIf<byte[], byte[]>> commitRecord = Collections.singletonList(new MockCommitRecord());

        // first record does not lead to commit
        handler.handleRecords(firstRecord);
        verify(target, times(0)).send(any(String.class), any(Integer.class));
        // second record does not lead to commit
        handler.handleRecords(secondRecord);
        verify(target, times(0)).send(any(String.class), any(Integer.class));
        // commit record leads to commit
        handler.handleRecords(commitRecord);
        verify(target, times(1)).send(any(String.class), any(Integer.class));
    }


    @Test
    public void testTransactionCloseWhenKupelaufnummerChanges() throws IOException {
        List<TopicRecordIf<byte[], byte[]>> topicRecords = readMultiZepasRecordsFrom("src/test/resources/four_transactions_should_close.json");

        handler.handleRecords(topicRecords);

        // should close and send 4 transaction with the data set available
        verify(target, times(4)).send(any(String.class), any(Integer.class));
    }




    private List<TopicRecordIf<byte[], byte[]>> readSingleZepasRecordFrom(String path) throws IOException {
        Path pathToRecord = Paths.get(path);
        ZepasDataRecord zepasDataRecord = objectMapper.readValue(pathToRecord.toFile(), ZepasDataRecord.class);
        return Collections.singletonList(new ZepasTopicRecord(zepasDataRecord));
    }

    private List<TopicRecordIf<byte[], byte[]>> readMultiZepasRecordsFrom(String path) throws IOException {
        Path pathToRecord = Paths.get(path);
        List<ZepasDataRecord> zepasDataRecords = Arrays.asList(objectMapper.readValue(pathToRecord.toFile(), ZepasDataRecord[].class));
        return zepasDataRecords.stream().map(ZepasTopicRecord::new).collect(Collectors.toList());
    }
}