package com.helvetia.tcc.connector;

import com.datamirror.ts.kafka.txconsistentconsumer.TopicRecordIf;
import com.datamirror.ts.kafka.txconsistentconsumer.TopicRecordType;
import com.datamirror.ts.kafka.txconsistentconsumer.TxConsistentConsumerConfiguration;
import com.datamirror.ts.kafka.txconsistentconsumer.TxConsistentConsumerException;
import com.datamirror.ts.kafka.txconsistentconsumer.api.TxConsistentConsumerImpl;
import com.datamirror.ts.kafka.txconsistentconsumer.bookmark.Bookmark;
import com.datamirror.ts.properties.TsPropertyException;
import com.datamirror.ts.util.Clock;
import com.datamirror.ts.util.trace.Trace;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.junit.jupiter.api.Test;

import java.nio.file.Paths;
import java.util.List;
import java.util.Properties;

public class PollTccIT {


    @Test
    public void pollTest() throws TxConsistentConsumerException, TsPropertyException {
        TxConsistentConsumerConfiguration tccConfig = initTccConfig();


        final TxConsistentConsumerImpl<byte[], byte[]> tcc = new TxConsistentConsumerImpl<>(tccConfig);
//        Bookmark startingPoint = Bookmark.getBookmarkBuilder("CDC-KAFKATEST1-ZEQ2KAFK-commitstream", tccConfig).head();
        Bookmark startingPoint = Bookmark.readExternal("000100244344432D4B41464B4154455354312D5A4551324B41464B2D636F6D6D697473747265616D00000000000B38E000000000");
        tcc.seek(startingPoint);
        while (true) {
            final List<TopicRecordIf<byte[], byte[]>> poll = tcc.poll(1000L);
            if (poll != null) {
                for (TopicRecordIf<byte[], byte[]> topicRecordIf : poll) {
                    final TopicRecordType recordType = topicRecordIf.getRecordType();
                    if (recordType.equals(TopicRecordType.COMMIT)) {
                        final Bookmark bookmark = topicRecordIf.getBookmark();
                        final ConsumerRecord<byte[], byte[]> consumerRecord = topicRecordIf.getConsumerRecord();
                        System.out.println("Got a Commit Record");
                        System.out.println("Bookmark: " + bookmark.get());

                        if (consumerRecord != null) {
                            System.out.println("timestamp: " + consumerRecord.timestamp());
                        } else {
                            System.out.println("consumerrecord is null");
                        }

                    }
                }
            }
        }

    }

    public static TxConsistentConsumerConfiguration initTccConfig() throws TsPropertyException {
        Properties tccConfiguration = new Properties();
        Trace.setTraceOnUntil(Clock.currentTimeMillis() + 30000);
        Trace.setEchoToConsole(true);
        final String confPath = Paths.get("src/test/resources/confIT/config.properties").toAbsolutePath().toString();

        tccConfiguration.setProperty("tx_consistent_consumer_config_file_path", confPath);

        tccConfiguration.setProperty("tx_consistent_consumer_topics_list", "CDC.zdkub000.z00.tkupe_person, CDC.zdkub000.z00.tkupr_privatperson,  CDC.zdkub000.z00.tkuun_unternehmung, CDC.zdkub000.z00.tkuad_adresse, CDC.zdkub000.z00.tkumi_mitarbeiter, CDC.zdkub000.z00.tkupz_perszus, CDC.zdkub000.z00.tkucp_codezurpers, CDC.zdkub000.z00.tkupi_pezusatzinfo, CDC.zdkub000.z00.tkupf_fmaregister, CDC.zdkub000.z00.tkuko_kommelement, CDC.zdkub000.z00.tkukb_koelverwbez");

        return new TxConsistentConsumerConfiguration(tccConfiguration);
    }

}
