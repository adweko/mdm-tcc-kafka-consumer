package com.helvetia.tcc.connector;

import com.datamirror.ts.kafka.txconsistentconsumer.TxConsistentConsumerException;
import com.datamirror.ts.kafka.txconsistentconsumer.bookmark.*;
import com.datamirror.ts.util.Clock;
import com.datamirror.ts.util.trace.Trace;
import org.apache.kafka.clients.consumer.*;
import org.apache.kafka.common.PartitionInfo;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.serialization.ByteArrayDeserializer;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.*;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.stream.Collectors;

public class ConsumeCommitStreamTopicTest {

    static Properties config = new Properties();
    static final Path confPath = Paths.get("src/test/resources/confIT/config.properties");

    @BeforeAll
    public static void setup() throws IOException {
        config.load(new FileInputStream(confPath.toFile()));
    }

    @Test
    public void consumeBookmarkTopic() throws IOException {
        Properties consumerConfig = (Properties) config.clone();
        consumerConfig.forEach((k,v) -> System.out.println(k + " " + v) );
        consumerConfig.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, ByteArrayDeserializer.class);
        consumerConfig.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, BookmarkDeserializer.class);
        Consumer<byte[], TxConsistentConsumerBookmark> consumer = new KafkaConsumer<>(consumerConfig);

        final String topic = "CDC-KAFKATEST1-ZEQ2KAFK-commitstream";

        final List<PartitionInfo> partitionInfos = consumer.partitionsFor(topic);
        final List<TopicPartition> topicPartitions = partitionInfos.stream()
                .map(info -> new TopicPartition(info.topic(), info.partition()))
                .collect(Collectors.toList());
        consumer.assign(topicPartitions);
        Map<TopicPartition, Long> endOffsets = consumer.endOffsets(consumer.assignment());

        consumer.assign(endOffsets.keySet());

        final ZonedDateTime zonedDateTime = LocalDateTime.of(2020, 8, 10, 15, 0, 0, 0).atZone(ZoneId.of("Europe/Berlin"));

        long timestampToSearchFor = Instant.from(zonedDateTime).toEpochMilli();
        final Map<TopicPartition, Long> topicPartitionToDesiredTimestamp = endOffsets.keySet().stream()
                .collect(Collectors.toMap(topicPartition -> topicPartition, topicPartition -> timestampToSearchFor));
        final Map<TopicPartition, OffsetAndTimestamp> topicPartitionToDesiredOffsetAndTimestamp = consumer.offsetsForTimes(topicPartitionToDesiredTimestamp);

        topicPartitionToDesiredOffsetAndTimestamp.forEach((tp, oat) -> consumer.seek(tp, oat.offset()));

        final int maxMessagesPrinted = 1;
        int messagesPrinted = 0;
        for (ConsumerRecord<byte[], TxConsistentConsumerBookmark> record : consumer.poll(Duration.ofSeconds(1L))) {
            if (messagesPrinted < maxMessagesPrinted) {

                TxConsistentConsumerBookmark txConsistentConsumerBookmark = BookmarkFactory.create(record.topic(), record.offset(), -1);
                System.out.println(txConsistentConsumerBookmark);
                System.out.println("timestamp: " + record.timestamp() + " (" + Instant.ofEpochMilli(record.timestamp()) + ") corresponds to Bookmark " + record.value());
                messagesPrinted += 1;
            }
        }

    }

    @Test
    public void testBookmarkFromCommitStream() throws TxConsistentConsumerException {

        Bookmark bookmark = Bookmark.readExternal("000100244344432D4B41464B41494E5447312D5A4551324B41464B2D636F6D6D697473747265616D00000000001EBF3C00000000");

        System.out.println(bookmark);
    }

}
