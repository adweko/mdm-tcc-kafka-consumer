# MDM TCC Kafka Consumer
Dieses Repository beinhaltet den MDM TCC Kafka Consumer.
Dieser Consumer (MDM-TCC) benutzt den IBM TCC Consumer (IBM-TCC), um Transaktionen aus ZEPAS korrekt zu lesen und an CVBoost weiterzureichen.

## Versionierung
Beim MDM-TCC handelt es sich im Java-Entwicklung, die standardmäßig über git/BitBucket versioniert werden kann.

## Deployment auf die Serverinstanzen
Der MDM-TCC wird lokal kompiliert und das fat jar auf einen (exakt einen, nie mehrere) der CVBoost-Server deployed.
Der MDM-TCC wird auf dem Server im Ordner "/opt/cvboostconnector" verwaltet und über den System-Service "cvboostconnector.service" gestartet und gestoppt.
Der Service verwendet hierzu "/opt/cvboostconnector/start.sh", was eine auf die Umgebung angepasste Version des Skripts unter "src/main/resources/start.sh" ist.

Zum Deployment wird der Service gestoppt, die aktuelle Version des mdmTCCKafkaConsumer.jar (in /target/) nach "/opt/cvboostconnector/" gelegt und der Service wieder gestartet. Sollten sich abhängige Bibliotheken geändert haben, ist der target/lib-Ordener ebenfalls nach "/opt/cvboostconnector/" zu kopieren.

Unter src/main/scripts/deploy_jar.sh finden sich die grundlegenden Befehle, um das main-jar zu deployen.
Ein Beispiel-Aufruf für die Umgebung DEVL wäre
```bash
# cd src/main/scripts/deployment
bash deploy_jar.sh -e env/DEVL-env.sh
```
Im Skript DEVL-env.sh werden einige Konstanten deklariert, die entsprechend je nach Umgebung und User
angepasst werden müssen, bevor das Deployment ausgeführt wird. 

Der MDMTCCKafkaConsumer wird als fat jar kompiliert und dieses deployed, um die Abhängigkeitsverwaltung einfach zu
gestalten. Das oben erwähnte Skript deployed das fat jat und benennt es konsistent zum Modulnamen auf der Zielumgebung um.



## Umgebungsspezifische Konfiguration

Der MDM-TCC erfordert, neben dem Hauptprogramm, einige Konfigurationsdateien.
Diese werden im Order "/opt/cvboostconnector/" erwartet, es handelt sich um
1. config.properties: Grundlegende Konfiguration, z.B. Kafka-Bootstrap-Server-Adresse.
2. log4j.properties: Konfiguration des Loggings.

Templates für diese Files werden unter src/main/resources bereitgestellt, diese müssen dann auf die jeweilige Umgebung angepasst werden.

## Bereitstellung TCC via Nexus
Zum Kompilieren ist es notwendig, die den IBM TCC beinhaltende jar für maven verfügbar zu machen, namentlich das ts.jar.
Neue Versionen des TCC kommen werden manuell an das Team/die Helvetia von IBM übermittelt, derzeit werden diese
im Projektordner des MDM-Teams abgelegt.
Die Versionen sind semantisch versioniert und mit einer Buildnummer versehen.
Zur Systematisierung der Abhängigkeiten wird das in diesen Distributionen enthaltene ts.jar ins
Nexus aufgenommen, via der Upload/Funktion unter 
https://nexus.helvetia.group/#browse/upload:thirdparty

Dabei sind die Identifier wie folgt zu befüllen:
- Extension: jar
- Group Id: com.ibm
- Artifact Id: tcc
- Version: VV.V.V.V-BBBB, z.B. 11.4.0.2-5544
- Generate POM: Haken setzen
- Packaging: jar

Das ts.jar oben unter "File" auswählen und dann auf Upload drücken.

## Offene Bugs


 
## Offene Punkte und Optimierungen
1. Deployment als fat jar zur Vermeidung potentieller Inkonsistenzen in den Abhängigkeiten / Vereinfachung des Prozesses
2. Automatisierung des Konfigurationsmanagements
3. Überarbeitung des Source-Codes:
    1. Refactoring
    2. Benutzen von zwei Dateien für das Bookmarking, falls eine während des Schreibvorgangs korrumpiert.
4. Aufnahme des IBM-TCC jars in Nexus zur Systematisierung der Abhängigkeitsverwaltung und Erleichterung des Ausrollens von Updates des IBM-TCC
5. Deployment der MDM-TCC-Distributionen über Nexus
6. Password und Username für CVBoost sind im Klartext in den properties files. 

